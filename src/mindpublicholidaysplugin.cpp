/*
    SPDX-FileCopyrightText: 2022 Hannah Kiekens <hannahkiekens@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "mindpublicholidaysplugin.h"

#include <QDateTime>

MindPublicHolidaysPlugin::MindPublicHolidaysPlugin(QObject *parent)
    : CalendarEvents::CalendarEventsPlugin(parent)
{
    // 2022
    addMindHoliday(2022, 3, 4, QStringLiteral("New Years Day replacement"));
    addMindHoliday(2022, 4, 18, QStringLiteral("Easter Monday"));
    addMindHoliday(2022, 5, 27, QStringLiteral("Labor Day replacement"));
    addMindHoliday(2022, 5, 26, QStringLiteral("Ascension Day"));
    addMindHoliday(2022, 6, 6, QStringLiteral("Whit Monday"));
    addMindHoliday(2022, 7, 21, QStringLiteral("Belgian National Holiday"));
    addMindHoliday(2022, 8, 15, QStringLiteral("Assumption of the Holy Virgin"));
    addMindHoliday(2022, 11, 01, QStringLiteral("All Saints' Day"));
    addMindHoliday(2022, 11, 11, QStringLiteral("Armistice Day"));
    addMindHoliday(2022, 10, 31, QStringLiteral("Christmas replacement"));

    // 2023
    addMindHoliday(2023, 5, 19, QStringLiteral("New Years Day replacement"));
    addMindHoliday(2023, 4, 10, QStringLiteral("Easter Monday"));
    addMindHoliday(2023, 5, 1, QStringLiteral("Labor Day"));
    addMindHoliday(2023, 5, 18, QStringLiteral("Ascension Day"));
    addMindHoliday(2023, 5, 29, QStringLiteral("Whit Monday"));
    addMindHoliday(2023, 7, 21, QStringLiteral("Belgian National Holiday"));
    addMindHoliday(2023, 8, 15, QStringLiteral("Assumption of the Holy Virgin"));
    addMindHoliday(2023, 11, 01, QStringLiteral("All Saints' Day"));
    addMindHoliday(2023, 8, 14, QStringLiteral("Armistice Day"));
    addMindHoliday(2023, 12, 25, QStringLiteral("Christmas replacement"));

}

MindPublicHolidaysPlugin::~MindPublicHolidaysPlugin() = default;

void MindPublicHolidaysPlugin::loadEventsForDateRange(const QDate &startDate, const QDate &endDate)
{
    if (m_lastStartDate == startDate && m_lastEndDate == endDate) {
        Q_EMIT dataReady(m_lastData);
        return;
    }

    QMultiHash<QDate, CalendarEvents::EventData> data;

    for (const auto &holiday : m_mindHolidays) {
        const auto holidayDate = holiday.startDateTime().date();
        if ((holidayDate >= startDate) && (holidayDate <= endDate)) {
            data.insert(holidayDate, holiday);
        }
    }

    m_lastStartDate = startDate;
    m_lastEndDate = endDate;
    m_lastData = data;

    Q_EMIT dataReady(data);
}

void MindPublicHolidaysPlugin::addMindHoliday(const int year, const int month, const int day, const QString holiday)
{
    CalendarEvents::EventData eventData;
    eventData.setStartDateTime(QDate(year, month, day).startOfDay());
    eventData.setEndDateTime(eventData.startDateTime());
    eventData.setIsAllDay(true);
    eventData.setTitle(holiday);
    eventData.setEventType(CalendarEvents::EventData::Holiday);
    eventData.setIsMinor(false);

    m_mindHolidays.append(eventData);
}
