/*
    SPDX-FileCopyrightText: 2022 Hannah Kiekens <hannahkiekens@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <CalendarEvents/CalendarEventsPlugin>
#include <QObject>

class MindPublicHolidaysPlugin : public CalendarEvents::CalendarEventsPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.CalendarEventsPlugin" FILE "mindpublicholidaysplugin.json")
    Q_INTERFACES(CalendarEvents::CalendarEventsPlugin)

public:
    explicit MindPublicHolidaysPlugin(QObject *parent = nullptr);
    ~MindPublicHolidaysPlugin() override;

    void loadEventsForDateRange(const QDate &startDate, const QDate &endDate) override;

private:
    void addMindHoliday(const int year, const int month, const int day, const QString holiday);

    QDate m_lastStartDate;
    QDate m_lastEndDate;
    QMultiHash<QDate, CalendarEvents::EventData> m_lastData;

    QVector<CalendarEvents::EventData> m_mindHolidays;
};
