# Mind Public Holidays Plugin
![Screenshot](screenshots/digital-clock.png)

## What?
This is a CalendarEventsPlugin for Plasma Workspace. It contains a hardcoded list of the public holidays as defined by the company I work for: [mind.be](https://www.mind.be/)

## Why?
* Because I can't find the info when I need it
* Also serves as a usefull example on how to make a plugin for Plasma Workspace

## Enable plugin
Right click on the "Digital Clock plasmoid" (the clock on the bottom right). Select "Configure Digital Clock". On the "Calendar tab" enable the "Mind Public Holidays" plugin.
![Screenshot](screenshots/plugin.png)

## Dirty install instructions
``` bash
mkdir build
cd build/
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install
```

## TODO
* Add 2023!
* Package for all the distro's (meh, how about Arch AUR and Kubuntu PPA?)

